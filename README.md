# Token decrypt and decode example in java

This project is an example to : 
1. Decrypt partsadvisor JWE token
2. Check JWS signature
3. Decode JWT

This project use [Nimbus-jose library] (https://connect2id.com/products/nimbus-jose-jwt)

Check JWETokenToolsTest

This project use maven : command mvn clean install
