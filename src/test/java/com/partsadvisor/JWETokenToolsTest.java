package com.partsadvisor;

import com.nimbusds.jose.JOSEException;
import junit.framework.Assert;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.HashMap;

class JWETokenToolsTest {

    String signerKey = ")Ksq96H%j6D!{b{9E59d)-UQPSrT~{Tq";
    String secretKey = ")Ksq96H%j6D!{b{9E59d)-UQPSrT~{Tq";

    @Test
    void testJWEDecoder() throws JOSEException, ParseException {

        // Generate a partsadvisor JWE

        String jweToken = JWETokenTools.generateJWEWithJWSAsPayload(signerKey, secretKey, "5a96d12da9c216b26e9ee758", new HashMap<String, String>() {{
            put("warehouseId", "warehouseIdValue");
            put("clientId", "990PKUS");
            put("Easy4DToken", "3037348F7F4B97468F552E457AADA5CD");
        }});

        // Decrypt JWE and decode JWS to get payload

        String payLoad = JWETokenTools.decryptJWEAndGetPayLoad(secretKey, signerKey, jweToken);
        System.out.println("Payload : " + payLoad);

        Assert.assertEquals("{\"sub\":\"5a96d12da9c216b26e9ee758\",\"properties\":{\"Easy4DToken\":\"3037348F7F4B97468F552E457AADA5CD\",\"clientId\":\"990PKUS\",\"warehouseId\":\"warehouseIdValue\"}}", payLoad);
    }
}
