package com.partsadvisor;

import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEEncrypter;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.JWEHeader.Builder;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.DirectDecrypter;
import com.nimbusds.jose.crypto.DirectEncrypter;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

import java.text.ParseException;
import java.util.Map;

public class JWETokenTools {

    public static String decryptJWEAndGetPayLoad(String secretKey, String signerKey, String jwe) throws ParseException, JOSEException {

        // First step : parse and descrypt JWE
        JWEObject jweObject = JWEObject.parse(jwe);
        jweObject.decrypt(new DirectDecrypter(secretKey.getBytes()));

        // Second step decode JWT and check signer
        SignedJWT signedJWT = jweObject.getPayload().toSignedJWT();
        if (!signedJWT.verify(new MACVerifier(signerKey))) {
            System.out.println("Warning check fail signer is not good !!");
        }

        return signedJWT.getPayload().toString();
    }

    public static String generateJWEWithJWSAsPayload(String signerKey, String secretKey, String subject, Map properties) throws JOSEException {

        // First step, create an HMAC-protected JWS object with some payload

        JWTClaimsSet.Builder claimsSet = new JWTClaimsSet.Builder();
        claimsSet.subject(subject);

        claimsSet.claim("properties",
            properties
        );

        SignedJWT jwsObject = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), claimsSet.build());
        jwsObject.sign(new MACSigner(signerKey));

        // Output in URL-safe format
        System.out.println("JWS generated : " + jwsObject.serialize());

        // Second step : create JWE with previous generate JWS

        JWEHeader jweHeader = (new Builder(JWEAlgorithm.DIR, EncryptionMethod.A128CBC_HS256)).contentType("JWT").build();
        JWEEncrypter jweEncrypter = new DirectEncrypter(secretKey.getBytes());
        // Put the jws as payload
        JWEObject jweObject = new JWEObject(jweHeader, new Payload(jwsObject.serialize()));
        jweObject.encrypt(jweEncrypter);

        System.out.println("JWE generated : " + jweObject.serialize());

        return jweObject.serialize();
    }

    private static void printUsage() {

        System.out.println("Usage : java .jar [secretKey] [signerKey] [jwe]");
    }

    public static void main(String... args) throws ParseException, JOSEException {

        if (args.length != 3) {
            System.out.println("args numbers should be 3");
            printUsage();
            return;
        }

        System.out.println(decryptJWEAndGetPayLoad(args[0], args[1], args[2]));
    }
}
